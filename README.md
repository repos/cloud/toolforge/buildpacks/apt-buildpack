# apt Buildpack

This is a [Cloud Native Buildpack](https://buildpacks.io/) that adds support for `apt`-based dependencies during both build and runtime.

This buildpack is based on the [fagiani/buildpack-apt](https://github.com/fagiani/apt-buildpack).


## Usage

This buildpack is not meant to be used on its own, and instead should be in used in combination with other buildpacks.

Include a list of `apt` package names to be installed in a file named `Aptfile`; be aware that line ending should be LF, not CRLF.

The buildpack automatically downloads and installs the packages when you run a build.


### Aptfile

    # you can list packages
    libexample-dev
    imagemagick

## License

MIT